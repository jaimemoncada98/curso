const {Sequelize, DataTypes, Op, QueryTypes } = require('sequelize');
const initModels = require("./models/init-models");
const sequelize = new Sequelize ('mysql://root:root@localhost:3306/sakila')
const dbContext = initModels(sequelize);

console.log(`Comienzo `)

// async function conUno(){
//     let row = await dbContext.language.findOne({ where: { language_id: { [Op.lt]: 10 } } })

//         console.log(row.toJSON())

// }

// conUno().then(()=> { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0)}, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))

async function conMuchos() {
    let rows = await dbContext.language.findAll({ where: { language_id: { [Op.lt]: 9 } } })
    rows.forEach(row => {
        console.log(row.toJSON())
    });
}
conMuchos().then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))

// async function numPaginas (page=0, limit= 20){
//     let rows = await dbContext.address.findAll({ offset: page*limit, limit, order: ['address', 'district']})
//     console.log(rows.map(row=> ({id: row.address_id, name: 'Direccion: ' + row.address + ' Distrito: ' + row.district})))
// }
// numPaginas(1,10).then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))

// async function conAsociaciones() {
//     let row = await dbContext.category.findByPk(1, { include: 'film_id_film_film_categories' })
//     let rows = row.film_id_film_film_categories
//     console.log({ id: row.category_id, name: row.name , films: rows.map(item => ({ id: item.film_id, name: item.title })) })
// }

// conAsociaciones().then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))

// async function insert() {
//     let row = await dbContext.language.build({language_id: 9, name: 'asdd' })
//     try {
//         await row.validate()
//     } catch (error) {
//         console.log('400 Datos inválidos')
//         console.log(error.errors) 
//         console.log({
//             type: "https://tools.ietf.org/html/rfc7231#section-6.5.1",
//             status: 400,
//             title: 'One or more validation errors occurred.',
//             errors: Object.assign({}, ...error.errors.map(item => ({ [item.path]: item.message })))
//         })
//         return;
//     }
    
//     await row.save()
//     await conMuchos()
//     console.log(row)
// }

// insert().then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))

// async function update() {
//     let row = await dbContext.language.findOne({ order: [['name', 'DESC']] })
//     console.log(row)
//     row.name = row.name.toUpperCase()
//     console.log('Guardo ...')
//     await row.save()
//     console.log(row)
//     // await conMuchos()
// }

// update().then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))

async function remove() {
    let row = await dbContext.language.findOne({ order: [['language_id', 'DESC']] })
    console.log(row)
    await row.destroy()
    console.log(row)
    await conMuchos()
}

remove().then(() => { console.log(`Termine ${new Date().toLocaleTimeString('es')}`); process.exit(0) }, err => console.error(`ERROR ${new Date().toLocaleTimeString('es')}: `, err))
