const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10;

async function encriptaPassword(password) {
    const salt = await bcrypt.genSalt(SALT_ROUNDS)
    const hash = await bcrypt.hash(password, salt)
    console.log(password + ' ' + hash)
    return hash
}

encriptaPassword("PABLOhola-12")