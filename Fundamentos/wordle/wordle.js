// import palabras from "./Palabras.js";

// const numeroDeIntentos = 6;
// let intentosRestantes = numeroDeIntentos;
// let intentoActual =[];
// let letraSiguiente = 0;
// let intentoCorrectoString = palabras[Math.floor(Math.random() * palabras.length)]
// console.log(intentoCorrectoString)

// function inicioTablero() {
//     let tablero = document.getElementById("tableroJuego");

//     for (let i = 0; i<numeroDeIntentos; i++){
//         let fila = document.createElement("div")
//         fila.className = "letra-fila"

//         for (let j = 0; j < 5; j++){
//             let caja = document.createElement("div")
//             caja.className = "letra-caja"
//             fila.appendChild(caja)
//         }
//         tablero.appendChild(fila)
        
//     }
// }

// function sombreaTeclado(letra,color){
//     for (const elem of document.getElementsByClassName("teclado-boton"))
//     if(elem.textContent === letra){
//         let colorAnterior = elem.style.backgroundColor
//         if (colorAnterior === 'green'){
//             return
//         }
//         if (colorAnterior === 'yellow' && color !=='green'){
//             return
//         }
//         elem.style.backgroundColor = color
//         break
//     }
// }

// function borrarLetra(){
//     let fila = document.getElementsByClassName("letra-fila")[6-intentosRestantes]
//     let caja = fila.children[letraSiguiente-1]
//     caja.textContent=""
//     caja.classList.remove("rellenar-caja")
//     intentoActual.pop()
//     letraSiguiente -=1
// }

// function comprobacion(){
//     let fila = document.getElementsByClassName("letra-fila")[6-intentosRestantes]
//     let compruebaString =''
//     let intentoCorrecto = Array.from(intentoCorrectoString)

//     for (const val of intentoActual){
//         compruebaString += val
//     }
//     if (compruebaString.length != 5){
//         TransformStream.error("Faltan letras")
//         return
//     }
//     if(!palabras.includes(compruebaString)){
//         TransformStream.error("Palabra no esta en la lista")
//         return
//     }

//     for (let i = 0; i<5; i++){
//         let colorLetra=''
//         let caja = fila.children[i]
//         let letra = intentoActual[i]

//         let posicionLetra = intentoCorrecto.indexOf(intentoActual[i])
//         if (posicionLetra ===-1){
//             colorLetra = 'gris'
//         }else{
//             if(intentoActual[i]=== intentoCorrecto[i]){
//                 colorLetra = 'verde'
//             }else{
//                 colorLetra = 'amarillo'
//             }
//             intentoCorrecto [posicionLetra] = '#'
//         }

//         let retraso = 250 * i
//         setTimeout(()=>{
//             animateCSS(caja, 'flipInX')
//             caja.style.backgroundColor = colorLetra
//             sombreaTeclado (letra,colorLetra)
//         }, retraso)
//     }

//     if (compruebaString === intentoCorrectoString){
//         TransformStream.acierto("Has acertado!")
//         intentosRestantes = 0
//         return
//     }else{
//         intentosRestantes -= 1;
//         intentoActual = [];
//         letraSiguiente = 0;

//         if (intentosRestantes === 0){
//             TransformStream.error("Has excedido el numero de intentos, se acabo")
//             TransformStream.info('La palabra correcta era: "${intentoCorrectoString}"')
//         }
//     }
// }

// function insertarLetra (teclaPulsada){
//     if (letraSiguiente === 5){
//         return
//     }
//     teclaPulsada = teclaPulsada.toLowerCase()

//     let fila = document.getElementsByClassName("letra-fila")[6-intentosRestantes]

//     let caja = fila.children[letraSiguiente]
//     animateCSS(caja, "pulse")
//     caja.textContent = teclaPulsada
//     caja.classList.add("rellenar-caja")
//     intentoActual.push(teclaPulsada)
//     letraSiguiente += 1
// }

// const animateCSS = (element, animation, prefix ='animate__') =>
// new Promise((resolve,reject)=>{
//     const animationName = '${prefix}${animation}';
//     const node = element
//     node.style.setProperty('--animate-duration', '0.3s');
//     node.classList.add('${prefix}animated', animationName);

//     function handleAnimationEnd (event){
//         event.stopPropagation();
//         node.classList.remove('${prefix}animated', animationName);

//         resolve('Animation ended');
//     }
//     node.addEventListener('animationend', handleAnimationEnd, {once: true});
// });

// document.addEventListener("keyup", (e) => {
//     if (intentosRestantes === 0){
//         return
//     }

//     let teclaPulsada = String (e.tecla)
//     if (teclaPulsada === "Del" && letraSiguiente !==0){
//         borrarLetra()
//         return
//     }

//     if (teclaPulsada === "Enter"){
//         comprobacion()
//         return
//     }

//     let encontrar = teclaPulsada.match(/[a-z]/gi)
//     if (!encontrar || encontrar.length > 1){
//         return
//     }else{
//         insertarLetra(teclaPulsada)
//     }
// })

// document.getElementById("teclado-cont").addEventListener("click", (e) => {
//     const target = e.target

//     if (!target.classList.contains("teclado-boton")){
//         return
//     }
//     let tecla = target.textContent

//     if (tecla === "Del"){
//         tecla = "Borrar"
//     }

//     document.dispatchEvent(new KeyboardEvent("keyup", {'tecla': tecla}))
// })

// inicioTablero();

import { WORDS } from "./words.js";

const NUMBER_OF_GUESSES = 6;
let guessesRemaining = NUMBER_OF_GUESSES;
let currentGuess = [];
let nextLetter = 0;
let rightGuessString = WORDS[Math.floor(Math.random() * WORDS.length)]

console.log(rightGuessString)

function initBoard() {
    let board = document.getElementById("game-board");

    for (let i = 0; i < NUMBER_OF_GUESSES; i++) {
        let row = document.createElement("div")
        row.className = "letter-row"
        
        for (let j = 0; j < 5; j++) {
            let box = document.createElement("div")
            box.className = "letter-box"
            row.appendChild(box)
        }

        board.appendChild(row)
    }
}

function shadeKeyBoard(letter, color) {
    for (const elem of document.getElementsByClassName("keyboard-button")) {
        if (elem.textContent === letter) {
            let oldColor = elem.style.backgroundColor
            if (oldColor === 'blue') {
                return
            } 

            if (oldColor === 'yellow' && color !== 'green') {
                return
            }

            elem.style.backgroundColor = color
            break
        }
    }
}

function deleteLetter () {
    let row = document.getElementsByClassName("letter-row")[6 - guessesRemaining]
    let box = row.children[nextLetter - 1]
    box.textContent = ""
    box.classList.remove("filled-box")
    currentGuess.pop()
    nextLetter -= 1
}

function checkGuess () {
    let row = document.getElementsByClassName("letter-row")[6 - guessesRemaining]
    let guessString = ''
    let rightGuess = Array.from(rightGuessString)

    for (const val of currentGuess) {
        guessString += val
    }

    if (guessString.length != 5) {
        toastr.error("Not enough letters!")
        return
    }

    if (!WORDS.includes(guessString)) {
        toastr.error("Word not in list!")
        return
    }

    
    for (let i = 0; i < 5; i++) {
        let letterColor = ''
        let box = row.children[i]
        let letter = currentGuess[i]
        
        let letterPosition = rightGuess.indexOf(currentGuess[i])
        // is letter in the correct guess
        if (letterPosition === -1) {
            letterColor = 'red'
        } else {
            // now, letter is definitely in word
            // if letter index and right guess index are the same
            // letter is in the right position 
            if (currentGuess[i] === rightGuess[i]) {
                // shade green 
                letterColor = 'blue'
            } else {
                // shade box yellow
                letterColor = 'yellow'
            }

            rightGuess[letterPosition] = "#"
        }

        let delay = 250 * i
        setTimeout(()=> {
            //flip box
            animateCSS(box, 'flipInX')
            //shade box
            box.style.backgroundColor = letterColor
            shadeKeyBoard(letter, letterColor)
        }, delay)
    }

    if (guessString === rightGuessString) {
        toastr.success("You guessed right! Game over!")
        guessesRemaining = 0
        return
    } else {
        guessesRemaining -= 1;
        currentGuess = [];
        nextLetter = 0;

        if (guessesRemaining === 0) {
            toastr.error("You've run out of guesses! Game over!")
            toastr.info(`The right word was: "${rightGuessString}"`)
        }
    }
}

function insertLetter (pressedKey) {
    if (nextLetter === 5) {
        return
    }
    pressedKey = pressedKey.toLowerCase()

    let row = document.getElementsByClassName("letter-row")[6 - guessesRemaining]
    let box = row.children[nextLetter]
    animateCSS(box, "pulse")
    box.textContent = pressedKey
    box.classList.add("filled-box")
    currentGuess.push(pressedKey)
    nextLetter += 1
}

const animateCSS = (element, animation, prefix = 'animate__') =>
  // We create a Promise and return it
  new Promise((resolve, reject) => {
    const animationName = `${prefix}${animation}`;
    // const node = document.querySelector(element);
    const node = element
    node.style.setProperty('--animate-duration', '0.3s');
    
    node.classList.add(`${prefix}animated`, animationName);

    // When the animation ends, we clean the classes and resolve the Promise
    function handleAnimationEnd(event) {
      event.stopPropagation();
      node.classList.remove(`${prefix}animated`, animationName);
      resolve('Animation ended');
    }

    node.addEventListener('animationend', handleAnimationEnd, {once: true});
});

document.addEventListener("keyup", (e) => {

    if (guessesRemaining === 0) {
        return
    }

    let pressedKey = String(e.key)
    if (pressedKey === "Backspace" && nextLetter !== 0) {
        deleteLetter()
        return
    }

    if (pressedKey === "Enter") {
        checkGuess()
        return
    }

    let found = pressedKey.match(/[a-z]/gi)
    if (!found || found.length > 1) {
        return
    } else {
        insertLetter(pressedKey)
    }
})

document.getElementById("keyboard-cont").addEventListener("click", (e) => {
    const target = e.target
    
    if (!target.classList.contains("keyboard-button")) {
        return
    }
    let key = target.textContent

    if (key === "Del") {
        key = "Backspace"
    } 

    document.dispatchEvent(new KeyboardEvent("keyup", {'key': key}))
})

initBoard();