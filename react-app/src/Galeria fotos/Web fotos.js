import React from "react";
import muro from "./muro.css"

export class Foto extends React.Component {
    constructor(props) {
        super(props);
        let { id, zoom } = props;
        this.id = id;
        this.state = { visible: false }
        this.descargar = this.descargar.bind(this)
        this.ocultar = this.ocultar.bind(this)
        this.zoom = zoom;
    }

    shouldComponentUpdate(next_props) {
        this.zoom = next_props.zoom;
        return true;
    }
    descargar() {
        this.setState((_) => {
            return { visible: true }
        }
        )
    }
    ocultar() {
        this.setState((_) => {
            return { visible: false }
        }
        )
    }
    render() {
        if (this.state.visible) {
            return <div className="fotos" onClick={this.ocultar}><img src={"https://picsum.photos/id/" + this.id + "/300/300"} /></div>
        } else {
            return <div className="fotos" onClick={this.descargar}>Dale click para descargar</div>
        }
    }

}

export  class WebFotos extends React.Component {
    constructor(props) {
        super(props);
        let { numero } = props;
        this.numero = numero;
        this.state = {
            zoom: 1,
        };
        this.amplia = this.amplia.bind(this);
        this.reduce = this.reduce.bind(this);
    }

    amplia() {
        this.setState((prev) => {
            return { zoom: +prev.zoom + 0.1 };
        });
    }

    reduce() {
        this.setState((prev) => {
            return { zoom: +prev.zoom - 0.1 }
        })
    }
    render() {
        let lista = [];
        for (let i = 0; i <= this.numero; i++) {
            lista.push(<Foto id={i + 122} key={i}></Foto>)
        }
        return <>
            <div className="botones"></div>
            <button onClick={this.amplia}>+</button>
            <button onClick={this.reduce}>-</button>
            <div className="muro" style={{ zoom: this.state.zoom }}>{lista}</div></>;
    }
}