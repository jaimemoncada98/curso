import './App.css';
import { ErrorBoundary, PageNotFound } from './comunes';
import Calculadora from './calculadora';
import Demos from './ejemplos';
import Formulario from './formulario';
import { Foto, WebFotos} from './Galeria fotos/Web fotos';
import PersonasRoute, { PersonasMnt, PersonasForm, PersonasList, PersonasView } from './personas';
import { BrowserRouter, Link, Navigate, Outlet, Route, Routes } from 'react-router-dom';
import { ContactoAdd, ContactoEdit, Contactos, ContactosConRutas, ContactosList, ContactoView } from './contactos';
import { ContadorStored } from './contadorStored';
import { Notificaciones } from './notificaciones';
import MainHeader from './main-header';


export default function App() {
  return (
    <BrowserRouter>
    <MainHeader/>
      <Notificaciones/> 
      <nav>
        <Link to="/">inicio</Link>&nbsp;|&nbsp;
        <Link to="/contactos">contactos</Link>&nbsp;|&nbsp;
        <Link to="/personas">personas</Link>&nbsp;|&nbsp;
        <Link to="/calculadora">calculadora</Link>&nbsp;|&nbsp;
        <Link to="/chisme/de/hacer/numeros">calculadora buena</Link>&nbsp;|&nbsp;
        <Link to="/react-app/src/Galeria fotos/Web fotos.js">WebFotos</Link>&nbsp;|&nbsp;
        <Link to="/formularios">formulario</Link>&nbsp;|&nbsp;
        <Link to="/falsa.html">html</Link>&nbsp;|&nbsp;
        <Link to="/cont">cont</Link>&nbsp;|&nbsp;
      </nav>
      <div className='container-fluid'>
        <ErrorBoundary>
          <Routes>
            <Route path='/' element={<Demos />} />
            <Route path='/chisme/de/hacer/numeros' element={<Calculadora />} />
            <Route path='/calculadora' element={<Navigate to='/calculadora' />} />
            <Route path='/formularios' element={<Formulario />} />
            <Route path='/react-app/src/Galeria fotos/Web fotos.js' element={<Foto />} />
            <Route path='/falsa.html' element={<WebFotos />} />
            {/* 
              /personas --> list
              /personas/1  --> view
              /personas/1/edit  --> edit
              /personas/add  --> add
             */}
            <Route path='/personas' element={<PersonasRoute />}>
              <Route index element={<PersonasMnt />} />
              <Route path='add' element={<PersonasMnt />} />
              <Route path=':id' element={<PersonasMnt />} />
              <Route path=':id/edit' element={<PersonasMnt />} />
            </Route>
            <Route path='/contactoss/*' element={<ContactosConRutas />} />
            {/* <Route path='/contactos' element={<Contactos />}>
              <Route index element={<ContactosList />} />
              <Route path='add' element={<ContactoAdd />} />
              <Route path=':id' element={<ContactoView />} />
              <Route path=':id/edit' element={<ContactoEdit />} />
            </Route> */}
            <Route path='/contactos' element={<Outlet />}>
              <Route index element={<ContactosList />} />
              <Route path='add' element={<ContactoAdd />} />
              <Route path=':id' element={<ContactoView />} />
              <Route path=':id/edit' element={<ContactoEdit />} />
            </Route>
            <Route path='/cont' element={<ContadorStored />} />


            <Route path='*' element={<PageNotFound />} />
          </Routes>
        </ErrorBoundary>
        <p>These two counters are each inside of their own error boundary. So if one crashes, the other is not affected.</p>

      </div>
    </BrowserRouter>
    
  )
}

// export default App;
