import Wrapper from "./componentes calculadora/Wrapper";
import Screen from "./componentes calculadora/Screen";
import ButtonBox from "./componentes calculadora/ButtonBox";
import Button from "./componentes calculadora/Button";
import React from "react";
import { Calculadora as Calc } from "./biblioteca"

const btnValues = [
    ["C", "Borrar", "?", "/"],
    [7, 8, 9, "*"],
    [4, 5, 6, "-"],
    [1, 2, 3, "+"],
    [0, ".", "="],
];



export default class Calculadora extends React.Component {
    constructor(props) {
        super(props)
        this.state = { pantalla: 0 }
        this.calc = new Calc(
            () => { },
            () => { }
        )

    }
    componentDidMount() {
        this.calc.onPantallaChange = pantalla => this.setState({ pantalla })
    }
    buttonClick(value) {
        if (0 <= value && value <= 9) {
            this.calc.ponDigito(value)
        }
        if (value == '.') {
            this.calc.ponComa(value)
        }
        if (value == 'Borrar') {
            this.calc.borrar(value)
        }
        if (value == 'C') {
            this.calc.inicia(value)
            value = 0
        }
        if ('+-*/='.indexOf(value) !== -1)
            this.calc.calcula(value)
        if ('?'.indexOf(value) !== -1)
            this.calc.cambiaSigno(value)
    }

    render() {
        return (
            <Wrapper>
                <Screen value={this.state.pantalla} />
                <ButtonBox>
                    {
                        btnValues.flat().map((btn, i) => {
                            return (
                                <Button
                                    key={i}
                                    className={btn === "=" ? "equals" : ""}
                                    value={btn}
                                    onClick={() => {
                                        this.buttonClick(btn)
                                    }}
                                />
                            );
                        })
                    }
                </ButtonBox>
            </Wrapper>

        )
    }
}
