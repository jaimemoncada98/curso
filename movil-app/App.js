import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button } from 'react-native';
import Demos from './components/demos';
import PilaScreen from './screens/PilaScreen';
import { Calculadora } from './components/biblioteca';

export default function App() {
  return (
    <NavigationContainer>
      <Demos />
    </NavigationContainer>
  );
}

