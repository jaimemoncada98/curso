import { Text, View, Button, TouchableOpacity } from 'react-native';
import React from "react";
import { Calculadora as Calc } from "./biblioteca"
import styles from './theme/style';

const btnValues = [
    ["C", "Borrar", "/"],

];

const btnValues2 = [

    [7, 8, 9, "*"],

];
const btnValues3 = [

    [4, 5, 6, "-"],

];
const btnValues4 = [

    [1, 2, 3, "+"],

];
const btnValues5 = [

    [".",0,"=","?"],

];

const BotonBox = ({ children }) => {
    return <View style={{ flexDirection: 'row' }}>{children}</View>;
};


const Screen = ({ value }) => {
    return (
        <Text style={styles.header}  >{value}</Text>

    );
};

const Wrapper = ({ children }) => {
    return <View style="wrapper">{children}</View>;
};


const Boton = ({ value, onClick }) => {
    return (
        <TouchableOpacity style={styles.containerFluid} onPress={onClick}  >
            <Text style={styles.texto}>
                {
                    value.toString()
                }
            </Text>
        </TouchableOpacity>
    );
};


export default class Calculadora extends React.Component {
    constructor(props) {
        super(props)
        this.state = { pantalla: 0 }
        this.calc = new Calc(
            () => { },
            () => { }
        )
    }
    componentDidMount() {
        this.calc.onPantallaChange = pantalla => this.setState({ pantalla })
    }
    BotonClick(value) {
        if (0 <= value && value <= 9) {
            this.calc.ponDigito(value)
        }
        if (value == '.') {
            this.calc.ponComa(value)
        }
        if (value == 'Borrar') {
            this.calc.borrar(value)
        }
        if (value == 'C') {
            this.calc.inicia(value)
            value = 0
        }
        if ('+-*/='.indexOf(value) !== -1)
            this.calc.calcula(value)
        if ('?'.indexOf(value) !== -1)
            this.calc.cambiaSigno(value)
    }

    render() {
        return (
            <Wrapper>
                <Screen value={this.state.pantalla} />
                <View style={{ flexDirection: 'column',fontSize: 20}}>
                    <BotonBox>
                        {
                            btnValues.flat().map((btn, i) => {
                                return (
                                    <Boton
                                        key={i}
                                        
                                        value={btn}
                                        onClick={() => {
                                            this.BotonClick(btn)
                                        }}
                                    />
                                );
                            })}
                    </BotonBox>
                    <BotonBox>{
                        btnValues2.flat().map((btn, i) => {
                            return (
                                <Boton
                                    key={i}
                                    
                                    value={btn}
                                    onClick={() => {
                                        this.BotonClick(btn)
                                    }}
                                />
                            );
                        })
                    }
                    </BotonBox>
                    <BotonBox>{
                        btnValues3.flat().map((btn, i) => {
                            return (
                                <Boton
                                    key={i}
                                   
                                    value={btn}
                                    onClick={() => {
                                        this.BotonClick(btn)
                                    }}
                                />
                            );
                        })
                    }
                    </BotonBox>
                    <BotonBox>{
                        btnValues4.flat().map((btn, i) => {
                            return (
                                <Boton
                                    key={i}
                                    
                                    value={btn}
                                    onClick={() => {
                                        this.BotonClick(btn)
                                    }}
                                />
                            );
                        })
                    }
                    </BotonBox>
                    <BotonBox>{
                        btnValues5.flat().map((btn, i) => {
                            return (
                                <Boton
                                    key={i}
                                
                                    value={btn}
                                    onClick={() => {
                                        this.BotonClick(btn)
                                    }}
                                />
                            );
                        })
                    }
                    </BotonBox>


                </View>

            </Wrapper>

        )
    }
}
