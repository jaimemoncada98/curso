import { StyleSheet, YellowBox } from "react-native";

const styles = StyleSheet.create({
    container: {
        flexWrap: 'wrap',
        backgroundColor: 'red',
        width: 10,
    },
    containerFluid: {
        flex: 1,
        flexWrap: 'wrap',
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignContent: 'center',
        elevation: 8,
        height: 50,
        backgroundColor: "#009688",
        borderRadius: 12,
        paddingVertical: 10,
        paddingHorizontal: 12,
        marginVertical: 3,
        marginHorizontal: 1,
    },
    item: {
        backgroundColor: 'yellow',
        padding: 20,
        marginVertical: 5,
    },
    texto: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: "center",
    },
    contactos: {
        justifyContent: 'center',
        fontSize: 47,
        backgroundColor: 'yellow',
        alignItems: 'center',
    },
    negrita: {
        fontWeight: 'bold',
    },
    cursiva: {
        fontWeight: 'italic',
    },
    error: {
        color: 'red',
    },
    header: {
        marginTop: '1%',
        marginBottom: 4,
        fontSize: 32,
        backgroundColor: '#345f',
        textAlignVertical: 'center',
        height: '25%',
        textAlign: 'center',
        borderRadius: 10,
    },
    title: {
        fontSize: 24,
    },
    BotonBox: {

        flexDirection: "column",
    },
})

export default styles